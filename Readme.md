# Problem

An ecommerce company wants to analyse purchase behaviour of its customer. And Wants to send Bulk purchase offers to User who have purchased over 500 units of product. 

# Solution

Building user purchase metric feature in a dashboard that gives information regarding the purchases of user so that the company could send the bulk offers

# Hypothesis

With user purchase metric in a dashboard company can send Bulk purchase offers to customers

# Experiment

Pull in Data from external sources regarding user purchases and parse and process it using DuckDb and generate user purchase metric


# Start Instructions
`docker-compose up rabbitmq`
`python3 -m pip install -r requirements.txt`
`python3 src/send.py` Start pushing data into rabbitmq
`python3 src/main.py` Start two threads one listening events from rabbit mq another one asynchronously logging data for metric table
