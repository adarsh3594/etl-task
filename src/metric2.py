#!/usr/bin/env python

#!/usr/bin/env python

from duckdbsingleton import Duckdb
import asyncio
import simplejson as json
from termcolor import colored
duck = Duckdb()

async def show_metric_2():
    while True:
        duck.lock.acquire()
        duck.connection.execute(f"SELECT avg_amount_spent_per_product,amount_spent_per_unit, user_id, strftime(purchase_date, '%d/%m/%Y') as purchase_date FROM user_purchase_metric ORDER BY purchase_date LIMIT 10")
        results = duck.connection.fetchall()
        if len(results) > 0: 
            for result in results: 
                print(colored(json.dumps({
                     "amt_spent_per_product": result[0],
                    "amt_spent_per_unit": result[1],
                    "user_id": result[2],
                    "purchase_date": result[3]
                })+ " metric 2", "blue"))
        duck.lock.release()
        await asyncio.sleep(10)