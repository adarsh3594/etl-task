#!/usr/bin/env python

from duckdbsingleton import Duckdb
import asyncio
import simplejson as json
from termcolor import colored
duck = Duckdb()


async def show_metric_1():

    while True:
        duck.lock.acquire()
        duck.connection.execute(f"SELECT total_amount_spent,number_of_units, user_id,strftime(purchase_date, '%d/%m/%Y') as purchase_date FROM user_purchase_metric ORDER BY purchase_date LIMIT 10")
        results = duck.connection.fetchall()
        if len(results) > 0: 
            for result in results: 
                print(colored(json.dumps({
                    "amount_spent": result[0],
                    "number_of_units": result[1],
                    "user_id": result[2],
                    "purchase_date": result[3]
                })+ " metric 1", "red"))
        duck.lock.release()
        await asyncio.sleep(10)
