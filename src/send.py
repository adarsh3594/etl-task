#!/usr/bin/env python
import pika
from pathlib import Path
import time 
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='events')
data_file = Path(Path(__file__, ).resolve().parent, "data.csv")

count = 0
if data_file.is_file() : 
    f = open(data_file,'r')
    while True:
        count = count +1
        line = f.readline()
        if count != 1: 
            channel.basic_publish(exchange='', routing_key='events', body=line)
        time.sleep(1)
        if not line:
            break


print(" Sent All Data to Rabbit mq")
connection.close()