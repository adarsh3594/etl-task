
import pika
from threading import Thread, Lock
from queue import Queue
import asyncio
from duckdbsingleton import Duckdb
from metric1 import show_metric_1
from metric2 import show_metric_2

duckdb = Duckdb()

def transform_events(body: str, duckdb: Duckdb):
    if len(str(body)) < 5:
        return
    try:
        [user_id, purchase_date, quantity_purchased, unit_price, product_id] = str(
            body).replace("b'", "").replace("\\n'", '').split(',')

        try:
            duckdb.lock.acquire()
            con.execute(
                f"SELECT * from user_purchases WHERE user_id='{user_id}' AND product_id='{product_id}' AND purchase_date='{purchase_date}'")
            items = con.fetchall()

            if len(items) == 0:
                print("Processingg", user_id, purchase_date,
                      quantity_purchased, unit_price, product_id)
                con.execute("""
                    INSERT INTO  user_purchases VALUES (?,?,?,?,?)
                """, [user_id, purchase_date, int(quantity_purchased), float(unit_price), product_id])
            con.execute(
                f"DELETE FROM user_purchase_metric WHERE user_id='{user_id}' AND strftime(purchase_date, '%d/%m/%Y') = strftime(CAST('{purchase_date}' as TIMESTAMP), '%d/%m/%Y')")
            con.execute(f"""
                INSERT INTO user_purchase_metric 
                (user_id, total_amount_spent,number_of_units,amount_spent_per_unit, avg_amount_spent_per_product,purchase_date) 
                SELECT
                user_id,
                SUM(unit_price * quantity_purchased) as total_amount_spent,
                SUM(quantity_purchased) as number_of_units,
                CAST (SUM(unit_price * quantity_purchased) / SUM(quantity_purchased) as DECIMAL(10,2)) as amount_spent_per_unit,
                CAST (SUM(unit_price * quantity_purchased) / COUNT(DISTINCT product_id) as DECIMAL(10,2)) avg_amount_spent_per_product,
                strftime(purchase_date, '%Y-%m-%d') as purchase_date
                FROM
                user_purchases
                WHERE user_id='{user_id}' AND strftime(purchase_date, '%d/%m/%Y') = strftime(CAST('{purchase_date}' as TIMESTAMP), '%d/%m/%Y') 
                GROUP BY user_id, strftime(purchase_date, '%Y-%m-%d')
            """)

        except Exception as e:
            print(e)
        finally:
            duckdb.lock.release()
    except:
        return


def fetch_rabbitmq_events(duckdb: Duckdb):

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='events')

    def callback(ch, method, properties, body):
        transform_events(body=body, duckdb=duckdb)

    channel.basic_consume(
        queue='events', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


t = Thread(target=fetch_rabbitmq_events, args=(duckdb,))


t.start()

con = duckdb.connection
# Create Tables
# duckdb.exec("""DROP TABLE IF EXISTS
#             user_purchases""")
# duckdb.exec("""DROP TABLE IF EXISTS
#             user_purchase_metric""")
duckdb.exec("""CREATE TABLE IF NOT EXISTS
            user_purchases(user_id VARCHAR,
            purchase_date TIMESTAMP,
            quantity_purchased INTEGER,
            unit_price DECIMAL(10, 2),
            product_id VARCHAR)""")
duckdb.exec("""CREATE TABLE IF NOT EXISTS 
            user_purchase_metric(user_id VARCHAR,
            total_amount_spent DECIMAL(10,2),
            amount_spent_per_unit DECIMAL(10, 2),
            number_of_units INTEGER,
            avg_amount_spent_per_product DECIMAL(10,2), 
            purchase_date TIMESTAMP,
           )""")

async def show_metrics():
    await asyncio.gather(show_metric_1(), show_metric_2())

asyncio.run(show_metrics())





t.join()
