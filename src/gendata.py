#!/usr/bin/env python
import simplejson as json
import uuid
import datetime
import random
from pathlib import Path
import os
data_csv = Path(Path(__file__, ).resolve().parent, "data.csv")


if data_csv.is_file():
    os.remove(data_csv.resolve())
f = open(data_csv, "a")

products = []
users = []
date_ranges = []
for i in range(100): 
    products.append({"product_id": str(uuid.uuid4()), "unit_price": f"{str(random.randint(100, 10000))}.{str(random.randint(0, 20))}"})
for i in range(100): 
    users.append({"user_id": str(uuid.uuid4())})

f.write("user_id,purchase_date,quantity_purchased,unit_price, product_id\n")




def create_increment(limit):
    starting= -1
    def inner():
        nonlocal starting

        if starting < limit -1:
            starting+=1
        else :
            starting =0
        return starting
    return inner
get_next_hour = create_increment(24)
get_next_second = create_increment(60)


for month in range(6,12): 
    for i in range(1,31): 
        for rep in range(15):    
            date = str(datetime.datetime(2022, month, i, get_next_hour(), get_next_second()))
            if len(date_ranges) == 1000 :
                break
            date_ranges.append(date)



for i in range(1000):
    index = random.randint(0,99)
    user_index = random.randint(0,100-1)
    date = date_ranges[i]
    selected_product = products[index]
    selected_user = users[user_index]["user_id"]
    unit_price = selected_product["unit_price"]
    product_id = selected_product["product_id"]
    f.write(f"{selected_user},{date},{str(random.randint(1, 500))},{unit_price} ,{product_id}\n")


f.close()
