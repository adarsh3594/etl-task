import duckdb
from threading import Lock
class Duckdb:
    lock= Lock()
    connection = None
    def exec(self,command): 
        try:
            self.lock.acquire()
            self.connection.execute(command)
        finally:
            self.lock.release()
    def __init__(self):
        self.connection = duckdb.connect(database='my-db.duckdb', read_only=False)
    _instance = None
    def __new__(cls):
        if not cls._instance:
            cls._instance = super(Duckdb, cls).__new__(
                                cls)
        return cls._instance




