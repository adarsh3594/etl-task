FROM python:3.10-buster

WORKDIR /app

COPY requirements.txt requirements.txt

RUN python3 -m pip install -r requirements.txt

COPY ./src ./src

CMD ["python3", "src/main.py"]

